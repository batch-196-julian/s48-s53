import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);

// clear the local storage of the user's information
	unsetUser();

	// localStorage.clear();

	// Placing the "setUser" function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render a different component 

	// By adding useEffect, this will allow the logout page to render first before triggering the useEffect which change 
	useEffect (() => {

		setUser({id: null})
	})
	return(
		<Navigate to="/login"/>

		)
};
