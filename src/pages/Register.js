import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	/*
		mini-activity: 5 mins
		1. Create useState for email, password1 and password2
		2. Initial value for all will be an empty string
		3. Create useState with isActive and setIsActive with initial value of false
		4. Import the react hook used
	*/

	// state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// const [password2, setPassword2] = useState('');
	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// check if values are successfully binded
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e){
		 // prevents page redirection via form submission
		 e.preventDefault();

		 fetch('http://localhost:4000/users/checkEmailExists', {
		 	method: 'POST',
		 	headers: {
		 		'Content-Type' : 'application/json'
		 	},
		 	body: JSON.stringify({
		 		email:email
		 	})
		 })
		 .then(res => res.json())
		 .then(data => {
		 	console.log(data);
		 	// result: boolean

		 	if(data){
		 		Swal.fire({
		 			title: "Duplicate email found",
		 			icon: "info",
		 			text: "The email that you're trying to register already exist"
		 		});

		 	} else {
		 		/*mini-activity: ends at 11:30 AM
					1. Input the correct options for the fetch method of register
					2. Print in the console the data being fetch
					3. Send your console output in Hangouts
		 		*/
		 		fetch('http://localhost:4000/users', {
		 			method: 'POST',
		 			headers: {
		 				'Content-Type' : 'application/json'
		 			},
		 			body: JSON.stringify({
		 				firstName: firstName,
		 				lastName: lastName,
		 				email: email,
		 				mobileNo: mobileNo,
		 				password: password
		 			})
		 		})
		 		.then(res => res.json())
		 		.then(data => {
		 			console.log(data);

		 			if(data.email){
		 				Swal.fire({
		 					title: 'Registration successful!',
		 					icon: 'success',
		 					text: 'Thank you for registering'
		 				});
		 				
		 				history("/login");

		 			} else {
		 				Swal.fire({
		 					title: 'Registration failed',
		 					icon: 'error',
		 					text: 'Something went wrong, try again'
		 				});
		 			};
		 		});
		 	};
		 });

		 // clear input fields
		 setEmail('');
		 setPassword('');
		 setFirstName('');
		 setLastName('');
		 setMobileNo('');

		 // alert('Thank you for registering');
	};


	// Syntax:
		// useEffect(() => {}, [])	
	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, firstName, lastName, mobileNo, password]);

	/*mini-activity: 15 mins
		1. refactor the form and add firstName, lastName and mobileNo
		2. Send your outputs in Batch Hangouts (img of register page)
	
	*/


	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{ isActive ?			
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Register
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Register
			</Button>
		}

		</Form>
		</>

	)
}

