import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import AppNavbar from './components/AppNavbar';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
 {/* <AppNavbar/>*/}
    <App />
  </React.StrictMode>
);

// const name = "Peter Parker";
// const element = <h1> Hello, {name} </h1>

// const user = {
//     firstName: 'Levi',
//     lastName: 'Ackerman'
// };

// const formatName = (user) => {
//     return user.firstName + ' ' + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>


// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(element);