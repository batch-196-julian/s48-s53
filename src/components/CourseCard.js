// import { useState} from 'react';
import {Card } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard(props) {
    // console.log(props);
    // console.log(typeof props)

    // object distructuring
    const {name, description, price, _id} = props.courseProp;

    // const [count, setCount] = useState(0);
    // const [seat, setSeat] = useState(10);
    // console.log(useState(0));

    // react hooks - useStates => store it state
    // syntax: 
    // const [getter, setter] = useState(initialGetterValue);
    // function enroll(){

    //     if(seat > 0){
    //         setCount(count + 1);
    //         setSeat(seat - 1)
    //         console.log(`Enrollees: ${count}`)
    //     }
    //     else {
    //         alert('No More Seats Available.')
    //     }
    // }

    return(
                <Card className="p-3 mb-3">
                    <Card.Body>
                        <Card.Title className = "fw-bold">{name}</Card.Title>
                        <Card.Subtitle><i>Description:</i></Card.Subtitle>
                        <Card.Text>{description}</Card.Text>        
                         <Card.Subtitle><i>Price:</i></Card.Subtitle>
                         <Card.Text>&#8369; {price}</Card.Text>
                        <Link className = "btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
                    </Card.Body>                 
                </Card>
    )   
};
