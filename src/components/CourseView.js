import { useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView(){

	const { user } = useContext(UserContext);
	const history = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);

	const { courseId } = useParams();

	const enroll = (courseId) => {
		fetch('http://localhost:4000/users/enroll', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				});

				history("/courses");

			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			};
		});
	};


	useEffect(() => {

		console.log(courseId);
		fetch(`http://localhost:4000/courses/getSinglecourse/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})
	},[courseId]);

	return (
			
		<Container className="p-5">
			<Row>
				<Col lg={{span:6, offset: 3}} >
				<Card>
				  <Card.Body className="text-center">
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
				    <Card.Text>&#8369; {price}</Card.Text>
				    <Card.Subtitle className="mb-2 text-muted">Class Schedule:</Card.Subtitle>
				    <Card.Text>8:00 AM - 5:00 PM</Card.Text>

				     {

				      user.id !== null ?
				    	<Button variant="primary" onClick={() => enroll(courseId)} block>Enroll</Button>
				    	:
				    	<Link className="btn btn-danger"  to="/login">Log In</Link>

				    }

				  </Card.Body>
				</Card>
				</Col>
			</Row>
		</Container>


		)
}